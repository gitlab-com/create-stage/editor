Milestone planning for the [Remote Development Group](https://about.gitlab.com/handbook/product/categories/#remote-development-group)

## Overview

https://gitlab.com/groups/gitlab-org/-/epics/5065+

### [Web IDE Iteration Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/7440987?label_name[]=Category%3AWeb%20IDE&milestone_title=00.0)

### [Workspaces Iteration Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/7440987?label_name[]=Category%3AWeb%20IDE&milestone_title=00.0)


## Key Product and Engineering Themes for 00.0

### Web IDE

### Workspaces

## Team Capacity

See [Workspaces Iteration Planning Report](https://remote-development-team-automation-gitlab-org-re-6f9ae1d77f25d4.gitlab.io/workspaces-iteration-planning-report-latest.html)

[Web IDE Iteration Planning Report](https://remote-development-team-automation-gitlab-org-re-6f9ae1d77f25d4.gitlab.io/web-ide-iteration-planning-report-latest.html)

## Planned Deliverables

### Web IDE

```glql
display: table
fields: title, epic, weight, state, assignee, updatedAt
query: group = "gitlab-org" AND label = ("group::remote development", "Category:Web IDE") AND milestone = "00.0" AND label in ("webide-workflow::prioritized", "webide-workflow::done")
```

See [Web IDE Iteration Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/7440987?label_name%5B%5D=Category%3AWeb%20IDE)

### Workspaces

```glql
display: table
fields: title, epic, weight, state, assignee, updatedAt
query: group = "gitlab-org" AND label = ("group::remote development", "Category:Workspaces") AND milestone = "00.0" AND label in ("workspaces-workflow::prioritized", "workspaces-workflow::done")
```

See [Workspaces Iteration Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/5283620?label_name%5B%5D=Category%3AWorkspaces)

## Checklist

**EM @adebayo_a**

- [ ] Get team velocity & capacity
- [ ] Define planned tech debt for milestone 
- [ ] Assign %00.0 milestone and ~Deliverable labels to issues based on velocity

**PM @michelle-chen**

- [ ] Define key themes

**UX @tvanderhelm**

- [ ] Update UX issues

**Stable counterparts (QA, TW, AppSec)**

## Notes

**Average Workspaces velocity over the last two milestones:** XX
**Average Web IDE velocity over the last two milestones:** XX

/assign @michelle-chen @adebayo_a
