## Remote Development Team Specific Onboarding

This checkist should be created no earlier than two weeks after the company onboaring start. At this point, you are more famialiar with our company values and the GitLab platform.

The goal of this is to highlight key areas to get familiar with as you join the Remote Development group which maintains the Web IDE and Workspaces.

### Team Overview

> [Group Direction Page](https://about.gitlab.com/direction/create/remote_development/)

- [ ] I understand the team’s mission and objectives.
- [ ] I understand the importance of enabling seamless, secure, and efficient remote contributions.
- [ ] I understand the two feature categories maintained by the team 

#### Web IDE

- [ ] Read the [blog post](https://about.gitlab.com/blog/2022/12/15/get-ready-for-new-gitlab-web-ide/) introducing the Web IDE: key features, improvements, and goals.
- [ ] Understand the [vision and roadmap](https://about.gitlab.com/direction/create/remote_development/web_ide/) for the Web IDE within the Remote Development team.
- [ ] Learn how to navigate and use the [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) for editing, committing, and merging code.

#### Workspaces

- [ ] Read the [blog post](https://about.gitlab.com/blog/2023/06/26/quick-start-guide-for-gitlab-workspaces/) introducing the Workspaces: key features, improvements, and goals.
- [ ] Understand the [vision and roadmap](https://about.gitlab.com/direction/create/remote_development/workspaces/) for Workspaces within the Remote Development team.
- [ ] Create and explore a Workspaces using the [example-app](https://gitlab.com/gitlab-org/workspaces/examples/example-nodejs-express-app). The blog post above should be a good quick starter.
- [ ] Engineer-facing docs for Workspaces are [here](https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-docs) and [here](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/workspaces/)
- [ ] [Setting Up Local Env](https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-docs/-/blob/main/doc/local-development-environment-setup.md) could be a good starting point for setting up local development env.


### Processes and Operations

#### Meetings & Communication

- [ ] Review the [schedule and objectives](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/#-group-meetings) of regular team meetings.
- [ ] Confirm you have access to the Team's YouTube [unfiltered playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRQhnSYRNh1s1mEUypx67-).
- [ ] Join or confirm you've been added to the following slack channels
    - [ ] #s_create
    - [ ] #g_create_remote_development
    - [ ] #g_create_remote_development-standup
    - [ ] #g_create_remote_development-social
    - [ ] #f_vscode_web_ide
    - [ ] #f_workspaces
- [ ] Raise an MR to add yourself to be assigned to the [Weekly Team Annoucements template](https://gitlab.com/gitlab-com/create-stage/remote-development/-/blob/main/.gitlab/issue_templates/weekly_team_announcement.md)

#### Planning Process
- [ ] I understand the use of an Iteration Planning Meeting
- [ ] I understand the use of a High Level Planning Meeting
- [ ] I have an idea on [weights to use](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/#-what-weights-to-use) when estimating issues

### Essential Tools & Repositories
> [See repositories](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/main/doc/repositories.md)
- [ ] Explore key repositories:
    - [ ] `gitlab-web-ide-vscode-fork`
    - [ ] `gitlab-web-ide`
    - [ ] `gitlab-vscode-extension`
    - [ ] `gitlab-workspaces-tools`
    - [ ] `gitlab-agent`
    - [ ] `gitlab-workspaces-proxy`
- [ ] Familiarize yourself with `gitlab-web-ide-vscode-fork`
- [ ] Install and use `gitlab-vscode-extension`

### End of issue
- [ ] With your onboarding buddy, identify a small bug or improvement and contribute to fixing it before closing this issue.
    - [ ] Join a pairing session
    - [ ] Join an Engineering Sync session
    - [ ] Raise and merge an MR with your contribution

At the end of this issue you'd have gone through key processes and documentation. That being said, please still endevour to give the [team's page](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/#overview) a read. It's our source of truth on the teams process.
