_**Please remove yourself from the assignees list after you have read the announcements. The last person to remove themselves should please close the issue.**_
### Remote Development Team Metrics
<!-- insert image here -->

### Company/Engineering Updates
<!-- insert annoucements here -->

## Darva's Announcements
<!-- insert announcements here -->

/confidential 
/assign @cindy-halim @Saahmed @zhaochen_li @vtak @cwoolley-gitlab @ealcantara @pslaughter @daniyalAD @JoFletcher @ashvins
/label ~Announcements ~"Weekly Team Announcements"
